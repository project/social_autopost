<?php

namespace Drupal\social_autopost\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Autopost.
 */
class SocialAutopostSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['social_autopost.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_autopost.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_autopost.settings');

    $form['facebook_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Facebook settings'),
      '#open' => TRUE,
    ];

    $form['facebook_settings']['page_id'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('PAGE ID'),
      '#default_value' => $config->get('page_id'),
      '#description' => $this->t('Copy your Page id here'),
    ];

    $form['facebook_settings']['page_token'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('PAGE token'),
      '#default_value' => $config->get('page_token'),
      '#description' => $this->t('Copy your facebook page access token here'),
    ];

    $form['twitter_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Twitter settings'),
      '#open' => TRUE,
    ];

    $form['twitter_settings']['consumer_api_key'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('consumer api key'),
      '#default_value' => $config->get('consumer_api_key'),
      '#description' => $this->t('Copy your Twitter consumer api key here'),
    ];

    $form['twitter_settings']['consumer_api_key_secret'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Consumer api key secret'),
      '#default_value' => $config->get('consumer_api_key_secret'),
      '#description' => $this->t('Copy your Twitter consumer api key secret here'),
    ];

    $form['twitter_settings']['access_token'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Access token'),
      '#default_value' => $config->get('access_token'),
      '#description' => $this->t('Copy your Twitter access token here'),
    ];

    $form['twitter_settings']['access_token_secret'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Access token secret'),
      '#default_value' => $config->get('access_token_secret'),
      '#description' => $this->t('Copy your Twitter access token secret here'),
    ];

    $form['field_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Field settings'),
      '#open' => TRUE,
    ];

    $form['field_settings']['facebook_boolean_field'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Share to Facebook boolean field'),
      '#default_value' => $config->get('facebook_boolean_field'),
      '#description' => $this->t('Enter machine name of your Facebook boolean field'),
    ];

    $form['field_settings']['twitter_boolean_field'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Share to Twitter boolean field'),
      '#default_value' => $config->get('twitter_boolean_field'),
      '#description' => $this->t('Enter machine name of your Twitter boolean field'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('social_autopost.settings')
      ->set('page_id', $values['page_id'])
      ->set('page_token', $values['page_token'])
      ->set('consumer_api_key', $values['consumer_api_key'])
      ->set('consumer_api_key_secret', $values['consumer_api_key_secret'])
      ->set('access_token', $values['access_token'])
      ->set('access_token_secret', $values['access_token_secret'])
      ->set('facebook_boolean_field', $values['facebook_boolean_field'])
      ->set('twitter_boolean_field', $values['twitter_boolean_field'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
