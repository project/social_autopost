DESCRIPTION:
------------
This module automatically shares newly created posts to the configured
facebook and twitter pages.

INSTALLATION:
-------------
1. Extract the tar.gz into your 'modules' or directory.
2. Install the module at extend' section.
Note: Make sure curl is installed in your system.

CONFIGURATION
-------------
This module needs API keys,tokens and secrets fom Facebook and Twiiter.
1. Get your facebook page id and the page token.Make sure page token is long-lived type.
   For this you need Graph Explorer tool: https://developers.facebook.com/docs/graph-api/explorer.
2. Get your twitter consumer API key,API key secret,Oauth access token and Ouath access token secret.
   Refer more here: https://developer.twitter.com/en/docs
3. Create two boolean field in a content type for facebook and twiiter share boolean field.
   Reuse this same fields over the content types you want to automatically share.
4. Enter the module configuration settings page (/soical-autoshare/config).
   Enter the fields and save configuration.
5. Create new content type with the above boolean fields and select the published field.
   Then save the content.

UNINSTALLTION:
--------------
1. Disable the module.

REQUIREMENTS
------------
1. curl

CREDITS:
--------
Twitter API code from James Mallison (found at https://github.com/j7mbo/twitter-api-php)